Ansible playbook for CentOS7 LAMP stack
This includes different modues:
- web
- db
- sftp
- phpmyadmin

You should be able to use these modules separately.

All the variables are stored in group_vars/all

This example has a mix of password randomly generated and some pre-set in the variable file.

This is one of my first works with Ansible and is mainly done to practise. You will see still some debug messages and files written in /tmp... again, all expected :)
 
